/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sobel.android.paymentform;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.sobel.android.attestedhttpservice.AttestedPost;
import com.sobel.android.attestedhttpservice.AttestedPostResponse;
import com.sobel.android.hiddenbufferservice.HiddenContentHandle;
import com.sobel.android.secureinputservice.SecureInputManager;
import com.sobel.android.secureinputservice.SecureInputView;
import com.sobel.android.secureinputservice.SecureInputViewListener;


/**
 * AttestedLoginActivity
 */
public class PaymentFormActivity extends Activity {
    /**
     * Called with the activity is first created.
     */

	private static final String TAG = "PaymentFormActivity";
	
	private static final String PAYMENT_URL = "http://10.0.2.2:9876/pay";
	
	private LinearLayout mRootLayout;
	
	private Handler mHandler;
	
	private static int UPDATE_INTERVAL = 250;
	private boolean mValidationStarted;
	
	private TextView mNameInput;
	private SecureInputView mNumberInput;
	private SecureInputView mExpiryInput;
	private SecureInputView mCvvInput;
	
	private TextView mErrorText;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRootLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.paymentform_activity, null);
        setContentView(mRootLayout);

        mHandler = new Handler();
        mValidationStarted = false;
        
        mNameInput = (TextView)mRootLayout.findViewById(R.id.nameInput);
        mNumberInput = (SecureInputView)mRootLayout.findViewById(R.id.numberInput);
        mExpiryInput = (SecureInputView)mRootLayout.findViewById(R.id.expiryInput);
        mCvvInput = (SecureInputView)mRootLayout.findViewById(R.id.cvvInput);
        
        mNumberInput.setListener(new MaybeStartValidation());
        mExpiryInput.setListener(new MaybeStartValidation());
        mCvvInput.setListener(new MaybeStartValidation());

        mErrorText = (TextView)mRootLayout.findViewById(R.id.error);
        mErrorText.setTextColor(Color.RED);
        
    }
    
    public void doSubmit(View v) {
		String name = mNameInput.getText().toString();
		HiddenContentHandle number = mNumberInput.getHiddenContentHandle();
		HiddenContentHandle expiry = mExpiryInput.getHiddenContentHandle();
		HiddenContentHandle cvv = mCvvInput.getHiddenContentHandle();
		CreditCard c = new CreditCard(name, number, expiry, cvv);

		if (!c.getValidation().isValid()) {
			handleSubmitInvalidCard(c.getValidation());
			return;
		}

		AttestedPost req = new AttestedPost.Builder()
			.url(PAYMENT_URL)
			.addParam("name", name)
			.addParam("number", number)
			.addParam("expiry", expiry)
			.addParam("cvv", cvv)
			.build();

		req.perform(new AttestedPost.ResponseListener() {
			public void onResponse(AttestedPostResponse r) {
				handleSubmitResponse(r);
			}
		}, new AttestedPost.ErrorListener() {
			public void onError(String msg) {
				handleSubmitError(msg);
			}
		});
    }
    
    private void flashToast(String t) {
    	Toast.makeText(getApplicationContext(), t, Toast.LENGTH_LONG).show();
    }
    
    private void handleSubmitInvalidCard(CreditCard.Validation v) {
    	flashToast("Invalid Card!!");
    }
    
    private void handleSubmitResponse(AttestedPostResponse r) {
    	if (r.getCode() != 200) {
    		flashToast("App Error: " + r.getStatusLine());
    	} else {
    		flashToast("$$$$$$$$$$$$$$$$");
    	}
    }
    
    private void handleSubmitError(String msg) {
    	flashToast("Req Error: " + msg);
    }
    
    public class MaybeStartValidation implements SecureInputViewListener {

		@Override
		public void onEditStart() {
			if (!mValidationStarted) {
				mHandler.post(new ValidateCard());
				mValidationStarted = true;
			}
		}

		@Override
		public void onEditStop() {}
    	
    }
   
    public class ValidateCard implements Runnable {

    	@Override
    	public void run() {
    		
    		String name = mNameInput.getText().toString();
    		HiddenContentHandle number = mNumberInput.getHiddenContentHandle();
    		HiddenContentHandle expiry = mExpiryInput.getHiddenContentHandle();
    		HiddenContentHandle cvv = mCvvInput.getHiddenContentHandle();
    		CreditCard c = new CreditCard(name, number, expiry, cvv);

    		CreditCard.Validation v = c.getValidation();
    		updateErrorBorders(v);
    		showMessages(v);
    		setCardType(c.getType());
    		
    		// Reschedule
    		mHandler.postDelayed(new ValidateCard(), UPDATE_INTERVAL);
    	}
    }
    
    public void updateErrorBorder(View v, boolean valid) {
    	if (valid) {
    		v.setBackgroundColor(Color.WHITE);
    	} else {
    		v.setBackground(getDrawable(R.drawable.border));
    	}
    }
    
    public void updateErrorBorders(CreditCard.Validation v) {
    	updateErrorBorder(mNumberInput, v.validNumber());
    	updateErrorBorder(mExpiryInput, v.validExpiry());
    	updateErrorBorder(mCvvInput, v.validCvv());
    }
    
    public void showMessages(CreditCard.Validation v) {
    	List<String> es = v.errors();
    	Iterator<String> it = es.iterator();

    	mErrorText.setText("");
    	boolean first = true;
    	while (it.hasNext()) {
    		String e = it.next();
    		CharSequence c = mErrorText.getText();
    		if (!first) {
    			mErrorText.setText(c + "\n" + e);
    		}  else {
    			mErrorText.setText(e);
    			first = false;
    		}
    	}
    }
    
    public void setCardType(CreditCard.Type t) {
    	View ccLogo = mRootLayout.findViewById(R.id.ccLogo);
    	switch (t) {
    	case VISA:
    		ccLogo.setBackground(getDrawable(R.drawable.visa));
    		break;
    	case AMEX:
    		ccLogo.setBackground(getDrawable(R.drawable.amex));
    		break;
    	case MASTERCARD:
    		ccLogo.setBackground(getDrawable(R.drawable.mastercard));
    		break;
    	default:
    		ccLogo.setBackground(getDrawable(R.drawable.unknown));
    	}
    }
}

