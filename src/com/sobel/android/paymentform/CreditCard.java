package com.sobel.android.paymentform;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.sobel.android.hiddenbufferservice.HiddenContentHandle;
import com.sobel.jebpf.EBPFInstruction;

public class CreditCard {

	private String mName;
	private HiddenContentHandle mNumber;
	private HiddenContentHandle mExpiry;
	private HiddenContentHandle mCvv;

	/* From stripe lib */
	public static final String[] PREFIXES_AMEX = {"34", "37"};
    public static final String[] PREFIXES_VISA = {"4"};
    public static final String[] PREFIXES_MASTERCARD = {"50", "51", "52", "53", "54", "55"};
	public enum Type {
		VISA,
		MASTERCARD,
		AMEX,
		UNKNOWN,
	}
	private Type mType;
	
	private Validation mValidation;
	
	public CreditCard(String name, HiddenContentHandle num, HiddenContentHandle exp, HiddenContentHandle cvv) {
		mName = name;
		mNumber = num;
		mExpiry = exp;
		mCvv = cvv;
		
		mType = learnType();
		
		mValidation = validate();
	}
	
	private Type learnType() {
		if (hasAnyPrefix(PREFIXES_AMEX)) {
			return Type.AMEX;
		} else if (hasAnyPrefix(PREFIXES_VISA)) {
			return Type.VISA;
		} else if (hasAnyPrefix(PREFIXES_MASTERCARD)) {
			return Type.MASTERCARD;
		} else {
			return Type.UNKNOWN;
		}
	}

	private boolean hasAnyPrefix(String[] prefixes) {
		String regexFormat = "^%s.*";
		int i;
		for (i=0;i<prefixes.length;i++) {
			if (mNumber.matchesPattern(Pattern.compile(String.format(regexFormat, prefixes[i])))) {
				return true;
			}
		}
		return false;
	}
	
	public Type getType() {
		return mType;
	}
	
	private Validation validate() {
		Validation v = new Validation();
				
		validateCard(v);
		validateExpiry(v);
		validateCvv(v);
		
		return v;
	}
	
	private void validateCard(Validation v) {
		// mType should already be set..
		if (mType == Type.UNKNOWN) {
			v.addNumberError("Unknown card type");
			return;
		}
		
		int expectedLength;
		if (mType == Type.AMEX) {
			expectedLength = 15;
		} else {
			expectedLength = 16;
		}
		if (mNumber.getLength() != expectedLength) {
			v.addNumberError("Wrong length; expected " + expectedLength);
		}

		// Do a luhn against our number...
		int luhnPasses = mNumber.runEBPF(EbpfLuhn.codeBytes);
		if (luhnPasses != 1) {
			v.addNumberError("Luhn check does not pass");
		}
	}
	
	private void validateExpiry(Validation v) {
		Pattern p = Pattern.compile("^\\d\\d/\\d\\d$");
		if (!mExpiry.matchesPattern(p)) {
			v.addExpiryError("Should be in format MM/YY");
		}
	}
	
	private void validateCvv(Validation v) {
		int expectedLength;
		if (mType == Type.AMEX) {
			expectedLength = 4;
		} else {
			expectedLength = 3;
		}
		if (mCvv.getLength() != expectedLength) {
			v.addCvvError("Wrong length; expected " + expectedLength);
		}
	}
	
	public Validation getValidation() {
		return mValidation;
	}
	
	public static class Validation {
		private List<String> mErrors;
		
		private boolean mNumberValid;
		private boolean mExpiryValid;
		private boolean mCvvValid;
		
		public Validation() {
			mErrors = new ArrayList<String>();
			
			mNumberValid = true;
			mExpiryValid = true;
			mCvvValid = true;
		}
		
		public void addCvvError(String e) {
			addError("CVV", e);
			mCvvValid = false;
		}
		
		public void addExpiryError(String e) {
			addError("Expiry", e);
			mExpiryValid = false;
		}
		
		public void addNumberError(String e) {
			addError("Number", e);
			mNumberValid = false;
		}
		
		private void addError(String tag, String e) {
			mErrors.add(tag + ": " + e);
		}
		
		public boolean isValid() {
			return validNumber() && validExpiry() && validCvv();
		}
		
		public boolean validNumber() {
			return mNumberValid;
		}
		
		public boolean validExpiry() {
			return mExpiryValid;
		}
		
		public boolean validCvv() {
			return mCvvValid;
		}
			
		public List<String> errors() {
			return mErrors;
		}
	}
	
}
